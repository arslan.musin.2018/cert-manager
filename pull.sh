#!/bin/bash
rm -rf cache
helm pull cert-manager --untar --destination cache/. --version 1.13.3 --repo "https://charts.jetstack.io"
rm -rf bundle/cert-manager
helm template cert-manager cache/cert-manager -f cache/cert-manager/values.yaml -f values.yaml --output-dir bundle/. --namespace cert-manager
